/**
 * Vuex 类型辅助工具
 */

import store from '@/store'
import { IModuleKeys } from '@/store/types'

export function vuexModuleState<
  T extends Record<string, any>,
  K extends keyof T
>(module: IModuleKeys, prop: K): T[K] {
  return store.state[module][prop]
}

export function vuexModuleDispatch<
  T extends Record<string, any>,
  K extends keyof T
>(module: IModuleKeys, action: K, payload?: Parameters<T[K]>[1]) {
  // @ts-ignore
  return store.dispatch(`${module}/${action}`, payload)
}
