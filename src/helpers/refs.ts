/**
 * Refs 类型辅助工具
 */

export function getRef<T = any>(refs: Record<string, any>, name: string): T {
  return refs[name]
}
