import Vue from 'vue'

import { vuexModuleDispatch, vuexModuleState } from '@/helpers'
import { IMainActions, IMainState } from '@/store/types'

export const mixinCount = Vue.extend({
  computed: {
    count() {
      return vuexModuleState<IMainState, 'count'>('main', 'count')
    }
  },
  methods: {
    handleIncrement() {
      vuexModuleDispatch<IMainActions, 'increment'>('main', 'increment')
    },
    handleDecrement() {
      vuexModuleDispatch<IMainActions, 'decrement'>('main', 'decrement')
    }
  }
})
