import Vue from 'vue'

export const mixinInit = Vue.extend({
  data() {
    return {
      title: '- -'
    }
  },
  onLoad() {
    this.init()
  },
  methods: {
    init() {
      uni.showLoading({ title: '加载中' })

      setTimeout(() => {
        this.title = 'Hello UniApp'
        uni.hideLoading()
      }, 1500)
    },
    handleJump() {
      uni.navigateTo({
        url: '/pages/other/index'
      })
    }
  }
})
