import { ActionContext, ActionTree, Module } from 'vuex'

export interface IMainState {
  count: number
}

export interface IMainActions extends ActionTree<IMainState, any> {
  /**
   * 加法
   */
  increment: (ctx: ActionContext<IMainState, any>) => void
  /**
   * 减法
   */
  decrement: (ctx: ActionContext<IMainState, any>) => void
}

const module: Module<IMainState, any> = {
  namespaced: true,
  state: {
    count: 0
  },
  actions: <IMainActions>{
    increment({ state }) {
      state.count++
    },
    decrement({ state }) {
      state.count--
    }
  }
}

export default module
