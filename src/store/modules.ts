import main from './modules/main'

export const modules = {
  main
}

export type IModuleKeys = keyof typeof modules
