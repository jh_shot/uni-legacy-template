# Uni Legacy Template

## 简介

Uni Legacy Template 是一个开源的小程序模版。

## 特性

- **技术栈**：使用 Vue2/UniApp 等前端前沿技术开发
- **TypeScript**：应用程序级 JavaScript 的语言

## 准备

- [Node](http://nodejs.org/) 和 [Git](https://git-scm.com/)
- [Vue2](https://cn.vuejs.org/v2/guide/)
- [UniApp](https://uniapp.dcloud.io/)

## 安装使用

- 获取项目代码

```bash
git clone https://gitee.com/jh_shot/uni-legacy-template.git
```

- 安装依赖

```bash
npm i
```

- 运行

```bash
npm run dev:mp-weixin
```

- 打包

```bash
npm run build:mp-weixin
```

## 注意事项

- 装上 volar 插件后更好的支持模板开发
- 不要用 pnpm 安装依赖，mac下开发者工具会报错
- components 目录下的组件会被自动引用，不用手动引入
- flyio 和 vuex 是模版自带的依赖，每次执行 `npm run uvm` 都会自动安装，所以也别浪费时间删了
- 不用配置 devtool，`@dcloudio/vue-cli-plugin-uni/lib/configure-webpack.js` 里已有默认配置
- 不要将 vue-cli 升级到 5，会有很多问题
- computed 需要标注返回类型，否则报变量找不到 [详见](https://github.com/vuejs/vue/issues/8625)
- 不要使用 nvue 编写页面，问题和限制都很多，而且已经不维护了
- 长列表不要使用 scroll-view，[性能问题](https://uniapp.dcloud.net.cn/component/scroll-view.html#webviewtips)

## 待处理
